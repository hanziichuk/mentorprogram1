/*
jQuery.extend(jQuery.validator.messages, {
    required: "Це обов'язкове поле.",
    remote: "Please fix this field.",
    url: "Please enter a valid URL.",
    number: "Будь ласка, введіть число. Наприклад, 14.7",
    equalTo: "Увідіть те саме значення, що і для попереднього поля.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Введіть не більше, ніж {0} символів."),
    minlength: jQuery.validator.format("Введіть не менше, ніж {0} символів."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Будь ласка, введіть значення рівне або менше за {0}."),
    min: jQuery.validator.format("Будь ласка, введіть значення рівне або більше за {0}.")
});

// user language hidden input
const $languageInput = $('#user_language');
// user registraion success message
const $alert = $('.alert');
// user registration form submit button
const $registerUserButton = $('#register_user_button');
var preferredLanguage = "UKRAINIAN";
const logInUserFormId = 'log_in_user_form';

$(document).ready(function () {

    validateUser();

    $('#registration_user_form').submit(function (e) {
        e.preventDefault();

        if ($('#registration_user_form').valid()) {
            $.ajax({
                type: "POST",
                url: "/singUp"
            });
        }

    });

});

/!**
 * Validates user registration form
 * @returns {*}
 *!/
function validateUser() {
    return $('#registration_user_form').validate({
        errorClass: "my_error_class",
        rules: {
            password: {
                required: true,
                minlength: 1,
                maxlength: 30
            },
            password_again: {
                equalTo: '#password'
            },
            name: {
                required: true,
                maxlength: 30,
                minlength: 3
            },
            age: {
                required: true,
                max: 100,
                min: 1
            }
        }
    });
}

*/
