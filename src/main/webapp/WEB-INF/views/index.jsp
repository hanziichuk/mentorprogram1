
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tesla</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.css">
    <link rel="stylesheet" href="https://unpkg.com/@bootstrapstudio/bootstrap-better-nav/dist/bootstrap-better-nav.min.css">
    <style><%@include file="../../css/styles.min.css" %></style>
</head>

<body style="background-color:rgb(0,0,0);color:rgb(255,255,255);">
<section class="d-flex flex-column justify-content-between" id="background">
    <div id="tesla-top">
        <nav class="navbar navbar-light navbar-expand-md">
            <div class="container-fluid"><a class="navbar-brand" href="#"></a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse"
                     id="navcol-1">
                    <ul class="nav navbar-nav mx-auto">
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="#">Model S</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="#">Model X</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="#">Model 3</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="#">Roadster</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="#">Energy</a></li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="nav-item" role="presentation"><a class="nav-link active" href="#">Shop</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link" href="#">SingIn</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <h1 class="text-center" data-aos="fade-up" data-aos-duration="700" data-aos-once="true" id="title">Tesla</h1>
        <h2 class="text-center" data-aos="fade-up" data-aos-duration="700" data-aos-once="true" id="sub-title">Roadster</h2>
    </div>
    <div id="tesla-bottom">
        <div class="container">
            <div class="row">
                <div class="col-8 offset-2">
                    <p><br>​The quickest car in the world, with record-setting acceleration, range and performance.<br><br></p>
                </div>
            </div>
            <div class="row">
                <div class="col" data-aos="fade-down" data-aos-duration="400">
                    <p class="p-top"><i class="icon ion-speedometer"></i>2.1 s</p>
                    <p class="p-bot" style="font-size:13px;">0-100 km/h</p>
                </div>
                <div class="col within-border" data-aos="fade-down" data-aos-duration="500">
                    <p class="p-top">+400km/h</p>
                    <p class="p-bot" style="font-size:13px;">Top Speed</p>
                </div>
                <div class="col" data-aos="fade-down" data-aos-duration="600">
                    <p class="p-top">1000 km</p>
                    <p class="p-bot" style="font-size:13px;">Kilometer Range<br></p>
                </div>
                <div class="col align-self-center" data-aos="fade-down" data-aos-duration="700"><button class="btn btn-primary btn-block reserve-bnt" type="button">Reserv Now</button></div>
            </div>
            <div class="row">
                <div class="col"><button class="btn btn-link btn-block arrow-btn" type="button"><i class="icon ion-ios-arrow-down" style="font-size:32px;"></i></button></div>
            </div>
        </div>
    </div>
</section>
<section data-aos="fade-down" data-aos-duration="700" id="video-and-states"><iframe width="560" height="315" allowfullscreen="" frameborder="0" src="https://www.youtube.com/embed/tw4jkyfY4HE?autoplay=1&amp;loop=1&amp;playlist=tw4jkyfY4HE&amp;controls=0&amp;showinfo=0&amp;rel=0"></iframe>
    <div id="roadster-characteristics">
        <table class="table">
            <thead style="padding-left:0;">
            <tr>
                <th colspan="2">Base Specs<br></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="cell-left">Acceleration 0-100&nbsp;km/h<br></td>
                <td class="cell-right">2.1 sec<br></td>
            </tr>
            <tr>
                <td class="cell-left">Acceleration 1/4&nbsp;mile<br></td>
                <td class="cell-right">8.8&nbsp;sec<br></td>
            </tr>
            <tr>
                <td class="cell-left">Top Speed<br></td>
                <td class="cell-right">Over&nbsp;400&nbsp;km/h<br></td>
            </tr>
            <tr>
                <td class="cell-left">Wheel Torque<br></td>
                <td class="cell-right">10,000&nbsp;Nm<br></td>
            </tr>
            <tr>
                <td class="cell-left">Kilometer Range<br></td>
                <td class="cell-right">1000&nbsp;kilometers<br></td>
            </tr>
            <tr>
                <td class="cell-left">Seating<br></td>
                <td class="cell-right">4<br></td>
            </tr>
            <tr>
                <td class="cell-left">Drive<br></td>
                <td class="cell-right">All-Wheel Drive<br></td>
            </tr>
            <tr>
                <td class="cell-left">Base Price<br></td>
                <td class="cell-right">$257,000<br></td>
            </tr>
            <tr>
                <td class="cell-left">Base Reservation<br></td>
                <td class="cell-right">$64,000<br></td>
            </tr>
            <tr>
                <td class="cell-left">Founders Series Price<br></td>
                <td class="cell-right">$322,000<br></td>
            </tr>
            <tr>
                <td class="cell-left">Founders Series Reservation<br></td>
                <td class="cell-right">$322,000<br></td>
            </tr>
            </tbody>
        </table>
    </div>
</section>
<section class="d-flex align-items-center" data-aos="fade-down" data-aos-duration="700" id="roadster-description">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h3>Designed for Performance and Aero Efficiency<br></h3>
            </div>
            <div class="col">
                <p>As an all-electric supercar, Roadster maximizes the potential of aerodynamic engineering—with record-setting performance and efficiency.<br></p>
            </div>
        </div>
    </div>
</section>
<section class="d-flex justify-content-center align-items-center" data-aos="fade-down" data-aos-duration="700" id="Interior">
    <figure class="figure"><img class="img-fluid figure-img" src="${pageContext.request.contextPath}/img/interior.jpg">
        <figcaption class="figure-caption"><br><span id="interion-span">Interior</span>The first supercar to set every performance record and still fit seating for four.<br><br></figcaption>
    </figure>
</section>
<section class="d-flex align-items-end" data-aos="fade-down" data-aos-duration="700" id="top-view">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h4>Glass Roof</h4>
                <p>A lightweight, removable Glass Roof stores in the trunk for an open-air, convertible driving experience.<br></p>
            </div>
        </div>
    </div>
</section>
<div class="footer-basic">
    <footer>
        <div id="verticale-line"></div><button class="btn btn-primary reserve-bnt" type="button">Reserv Now</button>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="#">Tesla @ 2018</a></li>
            <li class="list-inline-item"><a href="#">Privacy &amp; Legal</a></li>
            <li class="list-inline-item"><a href="#">Contract</a></li>
            <li class="list-inline-item"><a href="#">Careers</a></li>
            <li class="list-inline-item"><a href="#">Forums</a></li>
            <li class="list-inline-item"><a href="#">Locations</a></li>
            <li class="list-inline-item">United States</li>
        </ul>
    </footer>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js"></script>
<script src="https://unpkg.com/@bootstrapstudio/bootstrap-better-nav/dist/bootstrap-better-nav.min.js"></script>
<script src="${pageContext.request.contextPath}/js/script.min.js"></script>
</body>

</html>
