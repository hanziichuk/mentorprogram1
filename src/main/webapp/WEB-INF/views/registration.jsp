<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" scope="page"/>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="reg.css">
  <link rel="stylesheet" href=".css">
  <style><%@include file="../../css/registration.scss"%></style>
  <style><%@include file="../../css/registration.css"%></style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>

<body style="background-size:cover;" class="backgroundImage">
  <nav class="navbar navbar-expand-md navbar-dark" style="background-size:cover;">
    <div class="container">
      <a class="navbar-brand" href="${contextPath}/">
        <i class="fa d-inline fa-lg fa-cloud"></i>
        <b>Book Shop<br> </b>
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent"> </div>
    </div>
  </nav>
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="card text-white p-5 bg-regformcolor">
            <div class="card-body">
              <h1 class="mb-4 regForm">Sing In</h1>
              <form action="${contextPath}/singIn" method="POST">
                <div class="form-group">
                  <label>Email address</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter name"> </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="password" placeholder="Password"> </div>
                <div class="checkbox">
                  <label><input type="checkbox" value="" id="stay_logged">Залишатись в системі</label>
                </div>
                <button type="submit" class="btn btn-success">Login</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card text-white p-5 bg-regformcolor">
            <div class="card-body">
              <h1 class="mb-4">Sign Up</h1>
              <form id="registration_user_form" action="${contextPath}/singUp" method="POST">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter name">
                </div>
                <div class="form-group">
                  <label>Age</label>
                  <input type="text" class="form-control" name="age" placeholder="Enter age">
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="password" placeholder="Enter password">
                </div>
                <div class="form-group">
                  <label>Confirm password</label>
                  <input type="password" class="form-control" name="password_again" placeholder="Confirm password">
                </div>
                <button name="submit" type="submit" id="register_user_button" class="btn btn-warning">Register</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 bg-regformcolor
 text-white">
    <div class="container">
      <div class="row">
        <div class="text-center align-self-center col-md-12">
          <div class="my-3 row">
            <div class="col-4">
              <a href="https://www.facebook.com" target="_blank">
                <i class="fa fa-facebook fa-2x"></i>
              </a>
            </div>
            <div class="col-4">
              <a href="https://twitter.com" target="_blank">
                <i class="fa fa-twitter fa-2x"></i>
              </a>
            </div>
            <div class="col-4">
              <a href="https://www.instagram.com" target="_blank">
                <i class="fa fa-instagram fa-2x"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>

</html>