<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" scope="page"/>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style><%@include file="../../css/theme.scss"%></style>
    <style><%@include file="../../css/theme.css" %></style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-md bg-primary navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="#">
            <i class="fa d-inline fa-lg fa-cloud"></i>
            <b> Book Shop</b>
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
            <a class="btn navbar-btn ml-2 text-white btn-secondary" href="${contextPath}/singUp">
                <i class="fa d-inline fa-lg fa-user-circle-o"></i> Sign in
            </a>
        </div>
    </div>
</nav>
<div class="py-5 text-center" style="background-image: url('http://mashupcorner.s3-ap-southeast-1.amazonaws.com/wp-content/uploads/20170913133923/best-libraries.jpg'); background-size: cover">
    <div class="container py-5">
        <div class="row">
            <div class="col-md-12">
                <h1 class="display-1 text-center text-uppercase text-color
                     " contenteditable="true">
                    <span style="color:white;">Find </span>The best
                    <span style="color:white;">book </span>&nbsp;
                    <br> </h1>
                <h1 class="text-center text-uppercase heading_style" contenteditable="true">for your holiday</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="display-1 text-center text-uppercase text-color"><br> </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="">&nbsp;</h1>
            </div>
        </div>
    </div>
</div>
<div class="py-5 text-white bg-secondary">
    <div class="container">
        <div class="row">
            <div class="align-self-center p-5 col-md-6">
                <h1 class="mb-4">Carousel with controls and captions</h1>
                <p class="mb-5">Get a fluid web page working on all devices with the Bootstrap 4 grid system.&nbsp; Responsive design made intuitive and effective with Pingendo.</p>
                <a class="btn btn-lg btn-outline-light text-center btnGetResponse">View online shop
                    <br>
                </a>
            </div>
            <div class="col-md-6 p-0">
                <div class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <img src="${pageContext.request.contextPath}/img/110991D-swa.png" atl="first slide" class="d-block img-fluid w-100 mx-auto">
                            <div class="carousel-caption">
                                <h3>Dining room</h3>
                                <p>Good architecture, better food</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block img-fluid w-100" src="../../img/A4300-en-US-150px-01.jpg" data-holder-rendered="true">
                            <div class="carousel-caption">
                                <h3>Cigar room</h3>
                                <p>Enjoy our fine selection</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block img-fluid w-100" src="<c:url value='../../img/A4300-es-US-150px-01.jpg'/> data-holder-rendered=" true">
                            <div class="carousel-caption">
                                <h3>Relax area</h3>
                                <p>Take the time to chill</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<input id="btnGetResponse" type="button" value="Redirect" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="scripts.js">
    $.ajax({
        url: 'https://onfor.info/jquery-ajax-tipichnye-primery-vyzova-ajax/',
        success: function(data) {
            $('#somediv').html(data);
        }
    });


</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
