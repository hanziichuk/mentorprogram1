package com.MentoringProgram.servlet;

import com.MentoringProgram.entity.User;
import com.MentoringProgram.factory.ServiceFactory;
import com.MentoringProgram.service.CRUDService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SignUpServlet", urlPatterns = "/singUp")
public class SingUpServlet extends HttpServlet {

    private final ServiceFactory serviceFactory = ServiceFactory.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/views/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User user = new User();
        user.setAge(req.getParameter("age"));
        user.setName(req.getParameter("name"));
        user.setPassword(req.getParameter("password"));
        CRUDService<User> userService = serviceFactory.getCRUDUserService();
        userService.create(user);

        req.getRequestDispatcher("WEB-INF/views/index.jsp").forward(req,resp);

    }
}
