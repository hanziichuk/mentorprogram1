package com.MentoringProgram.servlet;

import com.MentoringProgram.entity.User;
import com.MentoringProgram.factory.ServiceFactory;
import com.MentoringProgram.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "SignInServlet", urlPatterns = "/singIn")
public class SingInServlet extends HttpServlet {

    private static final String USER = "user";
    private static final String USER_ID = "user_id";
    private final ServiceFactory serviceFactory = ServiceFactory.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean stay_logged = Boolean.getBoolean(req.getParameter("stay_logged"));
        UserService userService = serviceFactory.getUserService();
        Optional<User> user = userService.findByName(req.getParameter("name"), req.getParameter("password"));

        if (user.isPresent()) {
            authenticateUser(user.get(), stay_logged, req, resp);

            resp.setStatus(HttpServletResponse.SC_OK);
        } else {
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        req.getRequestDispatcher("/reg").forward(req, resp);
    }


    private void authenticateUser(final User user, final boolean stay_logged, final HttpServletRequest req, final HttpServletResponse resp) {
        final HttpSession session = req.getSession();
        session.setAttribute(USER, user);

        if (stay_logged) {
            final Cookie cookie = new Cookie(USER_ID, Long.valueOf(user.getId()).toString());
            cookie.setMaxAge(60 * 60 * 24 * 15);

            resp.addCookie(cookie);
        }
    }
}