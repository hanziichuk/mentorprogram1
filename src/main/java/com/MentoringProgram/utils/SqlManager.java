package com.MentoringProgram.utils;

import org.apache.log4j.Logger;

import java.util.ResourceBundle;

public enum SqlManager {

    USER_FIND_BY_ID("user.findById"),

    USER_FIND_BY_NAME("user.findByName"),

    USER_FIND_ALL("user.findAll"),

    USER_SAVE("user.save"),

    USER_UPDATE("user.update"),

    USER_REMOVE("user.remove"),

    BOOK_SAVE("books.save"),

    BOOK_REMOVE("books.remove"),

    BOOK_UPDATE("books.update"),

    BOOK_FIND_BY_ID("books.findById"),

    BOOK_FIND_ALL("books.findAll"),


    STAFF_FIND_ALL_DOCTORS("staff.findAllDoctors"),

    STAFF_FIND_ALL_NURSES("staff.findAllNurses"),

    STAFF_FIND_ALL("staff.findAll"),

    STAFF_CREATE_USER_AND_STAFF("staff.createUserAndStaff"),

    STAFF_DELETE_USER_AND_STAFF("staff.deleteUserAndStaffByStaffId"),

    STAFF_FIND_ALL_ASSIGNED_PATIENTS_BY_STAFF_ID("staff.findAllAssignedPatientsByStaffId"),

    STAFF_FIND_ALL_UNASSIGNED_PATIENTS_BY_STAFF_ID("staff.findAllUnassignedPatientsByStaffId"),

    STAFF_ASSIGN_PATIENT("staff.assignPatient"),

    STAFF_DELETE_FROM_STAFF_HAS_PATIENT_BY_ID("staff.deleteFromStaffHasPatientsById"),

    STAFF_FIND_BY_USER_ID("staff.findByUserId"),

    STAFF_UNASSIGN_PATIENT("staff.unassignPatient"),

    STAFF_READ("staff.read"),

    PATIENT_FIND_ALL("patient.findAll"),

    PATIENT_READ("patient.read"),

    PATIENT_SET_DIAGNOSIS("patient.setDiagnosis"),

    PATIENT_CREATE_USER_AND_PATIENT("patient.createUserAndPatient"),

    PATIENT_DELETE_USER_AND_PATIENT("patient.deleteUserAndPatientByPatientId"),

    PATIENT_DELETE_FROM_STAFF_HAS_PATIENT_BY_ID("patient.deteleFromStaffHasPatientsById"),

    PATIENT_FIND_ALL_UNDISCHARGED_PATIENTS_BY_STAFF_ID("patient.findAllUndischargedPatientsByStaffId"),

    PATIENT_DISCHARGE_BY_ID("patient.dischargeById"),

    PATIENT_FIND_BY_USER_ID("patient.findByUserId"),

    PRESCRIPTION_CREATE("prescription.create"),

    PRESCRIPTION_FIND_ALL_BY_STAFF_ID_PATIENT_ID("prescription.findAllByStaffIdAndPatientId"),

    PRESCRIPTION_FIND_ALL_BY_STAFF_ID("prescription.findAllByStaffId"),

    PRESCRIPTION_FIND_ALL_BY_PATIENT_ID("prescription.findAllByPatientId"),

    PRESCRIPTION_DELETE_BY_STAFF_ID("prescription.deleteByStaffId"),

    PRESCRIPTION_DELETE_BY_PATIENT_ID("prescription.deleteByPatientId");

    private String value;

    SqlManager(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    private static Logger log = Logger.getLogger(SqlManager.class);
    private final static ResourceBundle sqlBundle = ResourceBundle.getBundle("sqlqueries");

    public String getProperty() {
        return sqlBundle.getString(value);
    }

    @Override
    public String toString() {
        return value;
    }
}
