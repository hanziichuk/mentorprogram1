package com.MentoringProgram.dao.imp;

import com.MentoringProgram.dao.CRUDDao;
import com.MentoringProgram.entity.Book;
import com.MentoringProgram.persistence.ConnectionManager;
import com.MentoringProgram.transformer.BookTransformer;
import com.MentoringProgram.utils.SqlManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImp implements CRUDDao<Book> {

    private static final Logger LOGGER = Logger.getLogger(BookDaoImp.class);
    private static final String DB_CONNECTION_PROBLEM_MSG = "There was a problem connecting to the database";
    private static final String UPDATED_BOOK_MSG = "Book with title %s was updated successfully!";
    private static final String SUCCESS_BOOK_REMOVE_MSG = "Book with id %s was deleted successfully!";
    private static final String SUCCESS_BOOK_FIND_MSG = "Book with id %s was find successfully!";

    @Override
    public Book create(Book book) {
        try (Connection con = ConnectionManager.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(SqlManager.BOOK_SAVE.getProperty())) {
                statement.setString(1, book.getTitle());
                statement.setString(2, book.getDescription());
                statement.setString(3, book.getCategory().name());

                statement.executeUpdate();
            }
        } catch (SQLException e) {
            LOGGER.info(DB_CONNECTION_PROBLEM_MSG, e);
        }

        return book;
    }


    @Override
    public Book update(Book book) {
        try (Connection con = ConnectionManager.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(SqlManager.BOOK_UPDATE.getProperty())) {
                statement.setString(1, book.getTitle());
                statement.setString(1, book.getDescription());
                statement.setString(3, book.getCategory().name());
                int rowsUpdated = statement.executeUpdate();

                if (rowsUpdated > 0) {
                    LOGGER.info(String.format(UPDATED_BOOK_MSG, book.getTitle()));
                }
            }

        } catch (SQLException e) {
            LOGGER.info(DB_CONNECTION_PROBLEM_MSG, e);
        }
        return book;
    }

    @Override
    public void remove(Book book) {
        try (Connection con = ConnectionManager.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(SqlManager.BOOK_REMOVE.getProperty())) {
                statement.setInt(1, book.getId());
                int rowsDeleted = statement.executeUpdate();

                if (rowsDeleted > 0) {
                    LOGGER.info(String.format(SUCCESS_BOOK_REMOVE_MSG, book.getId()));
                }
            }

        } catch (SQLException e) {
            LOGGER.info(DB_CONNECTION_PROBLEM_MSG, e);
        }
    }

    @Override
    public Book findById(long id) {
        Book book = null;
        try (Connection con = ConnectionManager.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(SqlManager.BOOK_FIND_BY_ID.getProperty())) {
                statement.setLong(1, id);
                try (ResultSet resultSet = statement.executeQuery()) {

                    while (resultSet.next()) {
                        book = new BookTransformer().fromResultsSetToBook(resultSet);
                        LOGGER.info(String.format(SUCCESS_BOOK_FIND_MSG, id));
                    }
                }
            }

        } catch (SQLException e) {
            LOGGER.info(DB_CONNECTION_PROBLEM_MSG, e);
        }

        return book;
    }

    @Override
    public List<Book> findAll() {
        List<Book> books = new ArrayList<>();
        try (Connection con = ConnectionManager.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(SqlManager.BOOK_FIND_ALL.getProperty())) {
                try (ResultSet resultSet = statement.executeQuery()) {

                    while (resultSet.next()) {
                        Book book = new BookTransformer().fromResultsSetToBook(resultSet);
                        books.add(book);
                    }
                }
            }

        } catch (SQLException e) {
            LOGGER.info(DB_CONNECTION_PROBLEM_MSG, e);
        }

        return books;
    }
}
