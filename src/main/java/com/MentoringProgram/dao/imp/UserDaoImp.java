package com.MentoringProgram.dao.imp;

import com.MentoringProgram.dao.CRUDDao;
import com.MentoringProgram.dao.UserDao;
import com.MentoringProgram.entity.User;
import com.MentoringProgram.persistence.ConnectionManager;
import com.MentoringProgram.utils.SqlManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.MentoringProgram.transformer.UserTransformer.fromResultsSetToUser;

public class UserDaoImp implements CRUDDao<User>, UserDao {

    private static final Logger LOGGER = Logger.getLogger(UserDaoImp.class);
    private static final String DB_CONNECTION_PROBLEM_MSG = "There was a problem connecting to the database";
    private static final String UPDATED_USER_MSG = "User with name %s was updated successfully!";
    private static final String SUCCESS_USER_REMOVE_MSG = "User with id %s was deleted successfully!";
    private static final String SUCCESS_USER_FIND_MSG = "User with id %s was find successfully!";


    @Override
    public Optional<User> findByName(final String user_name, final String user_password) {
        User user = null;
        Connection con = ConnectionManager.getConnection();

        try (PreparedStatement statement = con.prepareStatement((SqlManager.USER_FIND_BY_NAME.getProperty()))) {
            statement.setString(1, user_name);
            ResultSet rs = statement.executeQuery();

            while (!rs.isLast()) {
                user = fromResultsSetToUser(rs);
            }

        } catch (SQLException e) {
            LOGGER.error(DB_CONNECTION_PROBLEM_MSG, e);
        }

        return Optional.ofNullable(user);
    }

    @Override
    public User create(User user) {
        try (Connection con = ConnectionManager.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(SqlManager.USER_SAVE.getProperty())) {
                statement.setString(1, user.getName());
                statement.setString(2, user.getAge());
                statement.setString(3, user.getPassword());
                statement.setString(4, user.getRole().name());

                statement.executeUpdate();
            }
        } catch (SQLException e) {
            LOGGER.error(DB_CONNECTION_PROBLEM_MSG, e);
        }

        return user;
    }

    @Override
    public User findById(long id) {
        User user = null;
        Connection con = ConnectionManager.getConnection();

        try (PreparedStatement statement = con.prepareStatement((SqlManager.USER_FIND_BY_ID.getProperty()))) {
            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {

                while (!resultSet.isLast()) {
                    user = fromResultsSetToUser(resultSet);
                    LOGGER.info(String.format(SUCCESS_USER_FIND_MSG, id));
                }
            }

        } catch (SQLException e) {
            LOGGER.info(DB_CONNECTION_PROBLEM_MSG, e);
        }

        return user;

    }

    @Override
    public User update(User user) {
        try (Connection con = ConnectionManager.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(SqlManager.USER_UPDATE.getProperty())) {
                statement.setString(1, user.getName());
                statement.setString(1, user.getAge());
                statement.setInt(3, user.getId());
                int rowsUpdated = statement.executeUpdate();

                if (rowsUpdated > 0) {
                    LOGGER.info(String.format(UPDATED_USER_MSG, user.getName()));
                }
            }
        } catch (SQLException e) {
            LOGGER.info(DB_CONNECTION_PROBLEM_MSG, e);
        }
        return user;
    }

    @Override
    public void remove(User user) {
        try (Connection con = ConnectionManager.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(SqlManager.USER_REMOVE.getProperty())) {
                statement.setInt(1, user.getId());
                int rowsDeleted = statement.executeUpdate();

                if (rowsDeleted > 0) {
                    LOGGER.info(String.format(SUCCESS_USER_REMOVE_MSG, user.getId()));
                }
            }

        } catch (SQLException e) {
            LOGGER.info(DB_CONNECTION_PROBLEM_MSG, e);
        }
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (Connection con = ConnectionManager.getConnection()) {
            try (PreparedStatement statement = con.prepareStatement(SqlManager.USER_FIND_ALL.getProperty())) {
                try (ResultSet resultSet = statement.executeQuery()) {

                    while (resultSet.next()) {
                        User user = fromResultsSetToUser(resultSet);
                        users.add(user);
                    }
                }
            }

        } catch (SQLException e) {
            LOGGER.info(DB_CONNECTION_PROBLEM_MSG, e);
        }

        return users;
    }
}

