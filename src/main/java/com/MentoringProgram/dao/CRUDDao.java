package com.MentoringProgram.dao;

import java.util.List;

public interface CRUDDao<T> {

    T create(T type);

    T findById(long id);

    T update(T type);

    void remove(T type);

    List<T> findAll();
}
