package com.MentoringProgram.dao;

import com.MentoringProgram.entity.User;

import java.util.Optional;

public interface UserDao {

    Optional<User> findByName(String name, String password);
}
