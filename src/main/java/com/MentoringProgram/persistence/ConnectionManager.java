package com.MentoringProgram.persistence;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private static Logger LOGGER = Logger.getLogger(ConnectionManager.class);
    private static String url = "jdbc:mysql://localhost:3306/books_shop";
    private static String user = "root";
    private static String pas = "root";

    private static Connection connection = null;

    public static Connection getConnection() {
        final String driver = "com.mysql.jdbc.Driver";
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        try {
            if (connection == null ||  connection.isClosed()) {
                initializeConnection();
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        LOGGER.info(connection);

        return connection;
    }

    private static void initializeConnection() {
        try {
            connection = DriverManager.getConnection(url, user, pas);
        } catch (SQLException e) {
            LOGGER.error(connection);
            e.printStackTrace();
        }
    }
}
