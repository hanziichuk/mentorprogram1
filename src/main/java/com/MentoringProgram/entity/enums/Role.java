package com.MentoringProgram.entity.enums;

public enum Role {

    ADMIN("ADMIN"), USER("USER"), MANAGER("MANAGER"), GUEST("GUEST");

    Role(String role) {
    }

    Role() {
    }
}
