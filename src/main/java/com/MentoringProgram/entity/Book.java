package com.MentoringProgram.entity;

import com.MentoringProgram.entity.enums.Category;

import java.util.Objects;

public class Book {

    private Integer id;
    private String title;
    private String description;
    private Category category;

    private User owner;


    public Book() {
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
        }

    @Override
    public String toString() {
        return "BookDao{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", category=" + category +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return Objects.equals(getId(), book.getId()) &&
                Objects.equals(getTitle(), book.getTitle()) &&
                Objects.equals(getDescription(), book.getDescription()) &&
                getCategory() == book.getCategory();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getTitle(), getDescription(), getCategory());
    }
}
