package com.MentoringProgram.entity;

import com.MentoringProgram.entity.enums.Role;

import java.util.List;
import java.util.Objects;

public class User {
    private Integer id;
    private String name;
    private String age;
    private String password;
    private Role role;
    private List<Book> books;
    private List<Order> orders;

    public User() {
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getId(), user.getId()) &&
                Objects.equals(getName(), user.getName()) &&
                Objects.equals(getAge(), user.getAge()) &&
                Objects.equals(getPassword(), user.getPassword()) &&
                Objects.equals(getBooks(), user.getBooks());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName(), getAge(), getPassword(), getBooks());
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
