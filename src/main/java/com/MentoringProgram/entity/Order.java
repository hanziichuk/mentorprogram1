package com.MentoringProgram.entity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class Order {

    private int id;
    private Long orderNumber;
    private BigDecimal orderTotal;
    private List<Book> books;


    public Order() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getId() == order.getId() &&
                Objects.equals(getOrderNumber(), order.getOrderNumber()) &&
                Objects.equals(getOrderTotal(), order.getOrderTotal()) &&
                Objects.equals(getBooks(), order.getBooks());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getOrderNumber(), getOrderTotal(), getBooks());
    }
}
