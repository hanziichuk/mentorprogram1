package com.MentoringProgram.service;

import java.util.List;

public interface CRUDService<T> {

    T create(T type);

    T findById(long id);

    T update(T type);

    void remove(T type);

    List<T> findAll();
}
