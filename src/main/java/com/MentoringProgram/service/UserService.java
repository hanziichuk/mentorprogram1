package com.MentoringProgram.service;

import com.MentoringProgram.entity.User;
import com.MentoringProgram.entity.enums.Role;

import java.util.Optional;

public interface UserService {

    void saveUser(User user, Role role);

    Optional<User> findByName(String name, String password);

}
