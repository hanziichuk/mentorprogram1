package com.MentoringProgram.service.impl;

import com.MentoringProgram.dao.CRUDDao;
import com.MentoringProgram.factory.DAOFactory;
import com.MentoringProgram.entity.Book;
import com.MentoringProgram.service.CRUDService;

import java.util.List;

public class BookServiceImpl implements CRUDService<Book> {
    private final DAOFactory factory = DAOFactory.getInstance();
    private CRUDDao<Book> bookDao;

    public BookServiceImpl() {
        this.bookDao = factory.getBookDao();
    }

    @Override
    public Book create(Book book) {
        return bookDao.create(book);
    }

    @Override
    public Book findById(long id) {
        return bookDao.findById(id);
    }

    @Override
    public Book update(Book book) {
        return bookDao.update(book);
    }

    @Override
    public void remove(Book book) {
        bookDao.remove(book);
    }

    @Override
    public List<Book> findAll() {
        return bookDao.findAll();
    }
}
