package com.MentoringProgram.service.impl;

import com.MentoringProgram.dao.CRUDDao;
import com.MentoringProgram.dao.UserDao;
import com.MentoringProgram.entity.User;
import com.MentoringProgram.entity.enums.Role;
import com.MentoringProgram.factory.DAOFactory;
import com.MentoringProgram.service.CRUDService;
import com.MentoringProgram.service.UserService;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements CRUDService<User>, UserService {

    private final DAOFactory factory = DAOFactory.getInstance();

    private CRUDDao<User> CRUDUserDao;
    private UserDao userDao;

    public UserServiceImpl() {
        this.CRUDUserDao = factory.getCRUDUserDao();
        this.userDao =factory.getUserDao();
    }

    @Override
    public User create(final User user) {
        user.setRole(Role.USER);

        return CRUDUserDao.create(user);
    }

    @Override
    public Optional<User> findByName(final String name, final String password) {
        return userDao.findByName(name, password);
    }

    @Override
    public User findById(long id) {
        return CRUDUserDao.findById(id);
    }

    @Override
    public User update(User user) {
        return CRUDUserDao.update(user);
    }

    @Override
    public void remove(User user) {
        CRUDUserDao.remove(user);
    }

    @Override
    public List<User> findAll() {
        return CRUDUserDao.findAll();
    }

    @Override
    public void saveUser(final User user, final Role role) {
        user.setRole(role);
        CRUDUserDao.create(user);
    }
}
