package com.MentoringProgram;

import com.MentoringProgram.dao.CRUDDao;
import com.MentoringProgram.dao.imp.UserDaoImp;
import com.MentoringProgram.entity.User;

import java.sql.SQLException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException {
        CRUDDao userDao = new UserDaoImp();
        User user = new User();
        user.setName("dawda");
        user.setAge("awdwad");
        user.setPassword("Asdawsd");
        userDao.create(user);
    }
}
