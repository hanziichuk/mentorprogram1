package com.MentoringProgram.filter;

import com.MentoringProgram.entity.User;
import com.MentoringProgram.entity.enums.Role;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Predicate;

//@WebFilter(filterName = "RedirectFilter", urlPatterns = "/reg")
public class CheckRoleFilter extends HttpFilter {

    private static final String USER = "user";
    private Predicate<User> equalsAdminRole = user -> Role.ADMIN.equals(user.getRole());

    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        Optional<User> sessionUser = getUserFromSession(req).filter(equalsAdminRole);
        if (sessionUser.isPresent()) {
            res.sendRedirect("WEB-INF/views/index.jsp");
        } else {
            res.sendRedirect("WEB-INF/views/registration.jsp");
        }

        return;
    }

    private void checkSessionUserIsCookieUser(final User sessionUser, final User cookieUser) {
        int compareResult = Long.compare(sessionUser.getId(), cookieUser.getId());

        if (compareResult != 0) {
            throw new IllegalStateException("Session user and cookie user are not equal");
        }
    }

    private Optional<User> getUserFromSession(HttpServletRequest req) {
        final HttpSession session = req.getSession();
        return Optional.ofNullable((User) session.getAttribute(USER));

    }

}
