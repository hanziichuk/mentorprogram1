package com.MentoringProgram.factory;

import com.MentoringProgram.entity.Book;
import com.MentoringProgram.entity.User;
import com.MentoringProgram.service.CRUDService;
import com.MentoringProgram.service.UserService;
import com.MentoringProgram.service.impl.BookServiceImpl;
import com.MentoringProgram.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

public class ServiceFactory {
    private static Logger log = Logger.getLogger(DAOFactory.class);
    private static ServiceFactory instance = new ServiceFactory();


    public static synchronized ServiceFactory getInstance() {
        return instance;
    }

    public CRUDService<User> getCRUDUserService() {
        return new UserServiceImpl();
    }

    public CRUDService<Book> getBookService() {return new BookServiceImpl();}

    public UserService getUserService() {return new UserServiceImpl();}

}
