package com.MentoringProgram.factory;

import com.MentoringProgram.dao.CRUDDao;
import com.MentoringProgram.dao.UserDao;
import com.MentoringProgram.dao.imp.BookDaoImp;
import com.MentoringProgram.dao.imp.UserDaoImp;
import com.MentoringProgram.entity.Book;
import com.MentoringProgram.entity.User;
import org.apache.log4j.Logger;

public class DAOFactory {
    private static Logger log = Logger.getLogger(DAOFactory.class);
    private static DAOFactory instance = new DAOFactory();

    private DAOFactory() {

    }


    /**
     * Returns the instance of DAOFactory.
     *
     * @return the instance of DAOFactory.
     */
    public static synchronized DAOFactory getInstance() {
        return instance;
    }

    /**
     * Returns UserDaoImpl instance.
     *
     * @return UserDaoImpl instance.
     */
    public CRUDDao<User> getCRUDUserDao() {
        return new UserDaoImp();
    }

    public UserDao getUserDao() {
        return new UserDaoImp();
    }

    /**
     * Returns UserDAOImpl instance.
     *
     * @return UserDAOImpl instance.
     */
    public CRUDDao<Book> getBookDao() {
        return new BookDaoImp();
    }

}
