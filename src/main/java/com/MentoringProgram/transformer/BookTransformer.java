package com.MentoringProgram.transformer;

import com.MentoringProgram.entity.Book;
import com.MentoringProgram.entity.enums.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookTransformer {

    public Book fromResultsSetToBook(final ResultSet resultSet) throws SQLException {

        Book book = null;

        if (resultSet.next()) {
            book = new Book();
            book.setId(resultSet.getInt("id"));
            book.setTitle(resultSet.getString("book_title"));
            book.setDescription(resultSet.getString("book_description"));
            book.setCategory(Category.valueOf(resultSet.getString("book_category")));
        }

        return book;
    }
}
