package com.MentoringProgram.transformer;

import com.MentoringProgram.entity.User;
import com.MentoringProgram.entity.enums.Role;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class UserTransformer {

    public static User fromResultsSetToUser(final ResultSet resultSet) throws SQLException {

        User user = null;

        if (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getInt("user_id"));
            user.setName(resultSet.getString("user_name"));
            user.setAge(resultSet.getString("user_age"));
            user.setPassword(resultSet.getString("user_password"));
            user.setRole(Role.valueOf(resultSet.getString("user_role")));
        }
        return user;
    }
}
