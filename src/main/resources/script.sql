CREATE SCHEMA IF NOT EXISTS BOOKS_SHOP;

USE BOOKS_SHOP;

CREATE TABLE IF NOT EXISTS books (
 book_id          INT AUTO_INCREMENT PRIMARY KEY,
 book_title       VARCHAR(20) NULL,
 book_description VARCHAR(20) NULL,
 book_category    VARCHAR(20) NULL
);

CREATE TABLE IF NOT EXISTS orders (
 order_id     INT AUTO_INCREMENT PRIMARY KEY,
 order_number VARCHAR(20) NULL,
 order_total  VARCHAR(20) NULL,
 book_id      INT,
 FOREIGN KEY (book_id) REFERENCES books (book_id)
);
CREATE TABLE IF NOT EXISTS users (
 user_id       INT AUTO_INCREMENT PRIMARY KEY,
 user_name     VARCHAR(256),
 user_age      VARCHAR(256),
 user_password VARCHAR(256),
 user_role     VARCHAR(64) NOT NULL,
 book_id       INT,
 FOREIGN KEY (book_id) REFERENCES books (book_id),
 order_id      INT,
 FOREIGN KEY (order_id) REFERENCES orders (order_id)
);